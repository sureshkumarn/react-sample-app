var path = require('path');

module.exports = function() {
	var clientSrcRootName = 'wwwroot';
    var client = './' + clientSrcRootName +'/';
    var app = 'app';

    var config = {
        client: client,
        index: path.join(client, 'index.html'),
        
        css: [
            client + '/css/*.css'
        ],

        sass: {
            in: app + '/sass/site.scss',
            watch: app + '/sass/**/*.scss',
            out: client + '/css'
        },
        port:1337
    }
    return config;
}