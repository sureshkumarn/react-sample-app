var React = require('react');

var FooterComponent = React.createClass({

     getInitialState: function(){
      return null;
     },

    componentWillMount: function(){
    },

    componentDidMount: function(){
     
    },

    render:function(){
        return(
      <footer className="footer">
                  <div>
                        <b>nskweb.in - &copy; Copyright 2016. All rights reserved</b>
                 </div>
           
      </footer>
        )
    }
});

module.exports = FooterComponent;