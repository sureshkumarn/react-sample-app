import React from 'react';
import {Link} from 'react-router';
import { browserHistory } from 'react-router';
import utils from '../../utils/commonUtils';

var LoginLinkComponent  = React.createClass({

    getInitialState: function(){
        return {
            isSessionValid:false
        };
    },
    componentWillMount: function(){},
    componentDidMount: function(){},
    logoutUser:function(e){
        e.preventDefault();
        utils.removeAccessKey();
    },
    renderLink: function(){
        var k, link="";
        k = this.state.isSessionValid;
        if(k){
            link = <Link onClick={this.logoutUser} to="">Logout</Link>;
        } else {
            link = <Link to="/login">Login</Link>;
        }
        return link;
    },
    render:function(){
        return(
            <div className="login-link navbar-nav nav">
                {this.renderLink()}
            </div>
        );
    }
});

module.exports = LoginLinkComponent;