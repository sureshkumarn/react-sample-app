import React from 'react';
import {Link} from 'react-router';
import { browserHistory } from 'react-router';
import LoginLinkComponent from './login-link.js';

var HeaderComponent  = React.createClass({

    getInitialState: function(){
      return null;
    },
    componentWillMount: function(){},
    componentDidMount: function(){},
    navHeaderLinks:function(){
        var navLinks = this.props.navLinks || [],
            nav_str = "";
            
        nav_str = navLinks.map(function (link) {
            return(
                <li key={link.href}><Link activeClassName="disabled selected" className="page-scroll font-bold" to={link.href}><strong>{link.text}</strong></Link></li>
            );
        });
        return nav_str;
    },
    render:function(){
        return(
            <header className="header" role="navigation">
               <div className="logo">
                    <a className="navbar-brand page-scroll">Sample React App!</a>
                </div>
                <div className="nav">
                    <ul className="nav navbar-nav">
                        {this.navHeaderLinks()}
                    </ul>
                    <LoginLinkComponent/>
                </div>
            </header>
        );
    }
});

module.exports = HeaderComponent;