var React = require('react');

var NotFound = React.createClass({

     getInitialState: function(){
      return null;
     },

    componentWillMount: function(){
    },

    componentDidMount: function(){
     
    },

    render:function(){
        return(
          <section>
            <h1>Page not Found!</h1>
            <p>The page you are looking for is not found!</p>
          </section>
        )
    }
});

module.exports = NotFound;