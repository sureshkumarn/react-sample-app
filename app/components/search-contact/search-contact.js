import config from '../../config/config';
import commonUtils from '../../utils/commonUtils';

var React = require('react');
var ReactDOM = require('react-dom');

var Modal = require('react-modal');
var _ = require('underscore');
var ONE=1;
var ZERO=0;
var RECORDS_PER_PAGE=10;

var SearchContact = React.createClass({
    getInitialState: function(){
      return (
        {
          contactListData:[],
          errorMsg:"",
          isLoading:false,
          messageType:""
        
          
          }
        );
    },

    initialSearchContactRequest:function(name,phone,email, guid){
        var data ={};
        var matchValues={};
        data["matchValues"]=({"name":name,"phone":phone,"email":email, guid:guid});
        data["start"]=1;
        data["pageSize"]=RECORDS_PER_PAGE;
        return data;
    },
    handleEnter:function(e){
        var self=this;
        if (e.charCode == 13) {
            self.handleSearchContact();
        }
    },

    searchContactList:function(name,phone,email, guid){
        var payloadData, api, method;

        if(commonUtils.isNullOrEmpty(guid)){ // regular search
            payloadData = this.initialSearchContactRequest(name,phone,email);
            payloadData = JSON.stringify(payloadData);
            api = config.apis.searchContactApi.url;
            method = config.apis.searchContactApi.options.method;
        } else { // guid search
            payloadData = "";
            api = config.apis.searchContactByGuidApi.url+guid.toString();
            method = config.apis.searchContactByGuidApi.options.method;
        }
        
        //console.log(api,payloadData);

        $.ajax({
            type: method,
            dataType:"json",
            url:api,
            headers:config.apis.searchContactApi.options.headers,
            data:payloadData,
                success: function(response) {
                    if(!commonUtils.isNullOrEmpty(guid)){
                        response = {
                                    "total":1,
                                    "start":1,
                                    "pageSize":10,
                                    "result":[response]};
                    }  
                    this.setState({
                        contactListData:response,
                        isLoading:false,
                        errorMsg:""
  
                    });
                    if(this.state.contactListData.total==0){
                        this.setState({
                        errorMsg:config.messages.NO_RECORDS_FOUND,
                        messageType:config.constants.INFO,
                        isLoading:false
                    });
                 }
              }.bind(this),

              error: function(xhr, status, err) {         
                  this.setState({
                  errorMsg:config.messages.NO_RECORDS_FOUND,
                  messageType:config.constants.INFO,
                  isLoading:false
                });
              }.bind(this)
        });
    },

    handleClear:function(){
        if(this.state.errorMsg !=""){  
            this.setState({errorMsg:""})
        }
    },

    handleSearchContact:function(e){
        var name =this.refs.fullname.value,
            phone=this.refs.phone.value,
            email=this.refs.email.value,
            guid = this.refs.guid.value;

        this.setState({errorMsg:"", isLoading:true,contactListData:[]});

        if(commonUtils.isNullOrEmpty(name) && commonUtils.isNullOrEmpty(phone) && commonUtils.isNullOrEmpty(email)){
            if(!commonUtils.isNullOrEmpty(guid)){
                this.setState({
                    guid:guid
                });
                this.searchContactList(name,phone,email, guid);
            } else {
                this.setState({errorMsg:config.messages.SEARCH_WARNING, isLoading:false,messageType:config.constants.WARNING})
            }
        }else if(!commonUtils.isNullOrEmpty(name) && name.length < 3){
            this.setState({errorMsg:config.messages.NAME_SEARCH_WARNING, isLoading:false,messageType:config.constants.WARNING})
        }else{
            this.setState({
                name:name,
                phone:phone,
                email:email
            });
            this.searchContactList(name,phone,email, guid);
        }
    },

    render:function(){
        var contentList;
        var loader;
        
        if(this.state.isLoading){
          loader=<Loader isLoading={this.state.isLoading} />
        }
        
        if(this.state.errorMsg != ""){
            contentList= <Error errorMsg={this.state.errorMsg} messageType={this.state.messageType}/>
        }else if(this.state.contactListData != null && this.state.contactListData.total > 0){
              contentList= <ContactDetailsComponent {...this.state} {...this.props} />
        }

        return(

                   <div className="merge-contact">
                       <div className="header-label-size">Search Contact</div>
                            <div className="search-input-form">
                                <div className="col-md-3">
                                    <input type="text" placeholder="GUID..." onFocus={this.handleClear} onKeyPress={this.handleEnter} className="form-control search-field" ref="guid" id="guid" />
                                </div>
                                <div className="col-md-3">
                                    <input type="text" placeholder="FullName..." onFocus={this.handleClear} onKeyPress={this.handleEnter} className="form-control search-field" ref="fullname" id="fullname" />
                                </div>
                                <div className="col-md-3">
                                    <input type="text" placeholder="Phone..." onFocus={this.handleClear} onKeyPress={this.handleEnter} className="form-control search-field" ref="phone" id="phone" />
                                </div>
                                <div className="col-md-3">
                                    <input type="text" placeholder="Email..." onFocus={this.handleClear} onKeyPress={this.handleEnter} className="form-control search-field" ref="email" id="email" />
                                </div>
                                <div className="col-md-3">
                                        <button type="button" onClick={this.handleSearchContact} className="btn">Search Contact</button>
                                </div>
                                                               
                            </div>
                            <div className="margin-bottom">{loader}
                      </div>
                      {contentList}

                  </div>
    )

  }
});
var ContactDetailsComponent=React.createClass({

  getInitialState:function(){
      
      return ({

        contactListData:this.props.contactListData,
        errorMsg:"",
        pageNo:1,
        isLoading:false,
        isLoadingContact:false,
        totalPage:commonUtils.totalPages(this.props.contactListData.total,RECORDS_PER_PAGE),
        isViewContact:false,
        contactDetailData:{}
      });

    },

  createSearchContactRequestPayload:function(currentPageNo){

    var data ={};
    var matchValues={};
    data["matchValues"]=({"name":this.props.name,"phone":this.props.phone,"email":this.props.email});
    data["start"]=currentPageNo;
    data["pageSize"]=RECORDS_PER_PAGE;
    return data;
  },

    searchContactList:function(currentPageNo){
        var payloadData;


        payloadData=this.createSearchContactRequestPayload(currentPageNo);

        $.ajax({
             type:"POST",
             dataType:"json",
             url:config.apis.searchContactApi.url,
             headers:config.apis.searchContactApi.options.headers,
             data:JSON.stringify(payloadData),
            success: function(response) {  
                 this.setState({
                  contactListData:response,
                  errorMsg:"",
                  isLoading:false,
                  pageNo:currentPageNo
        
                });
                 if(this.state.contactListData.total==0){
                  this.setState({
                  errorMsg:config.messages.NO_RECORDS_FOUND,
                  messageType:config.constants.INFO,
                  isLoading:false
                  });
                 }
              }.bind(this),

              error: function(xhr, status, err) {         
                  this.setState({
                  errorMsg:config.messages.NO_RECORDS_FOUND,
                  messageType:config.constants.INFO,
                  isLoading:false
                });
              }.bind(this)
          });  

  },

  handleCancelContactDetails:function(){
    this.setState({
      isViewContact:false
    })
  },
  handleCreateContact:function(){
   this.setState({
        isViewContact:true,
        panelHead:config.constants.CREATE_CONTACT,
        contactDetailData:{}
    })
  } ,

  handleViewContact:function(contactId,e){
   this.setState({isLoadingContact:true});
   $.ajax({
             type:"GET",  
             dataType:"json",
             url:config.apis.updateContactApi.url+contactId,
             headers:config.apis.searchContactApi.options.headers,
             success: function(response) {  
                 this.setState({
                  contactDetailData:response,
                  errorMsg:"",
                  isViewContact:true,
                  panelHead:config.constants.VIEW_CONTACT,
                  isLoadingContact:false,
                 });
                 if(this.state.contactDetailData.total==0){
                  this.setState({
                   errorMsg:config.messages.NO_RECORDS_FOUND,
                  messageType:config.constants.INFO,
                  isLoadingContact:false
                  });
                 }
              }.bind(this),

              error: function(xhr, status, err) {         
                  this.setState({
                   errorMsg:config.messages.NO_RECORDS_FOUND,
                  messageType:config.constants.INFO,
                  isLoadingContact:false
                });
              }.bind(this)
          }); 
    },

  handleNextPage:function(){

    var self=this;
    var currentPageNo=this.state.pageNo;
    if(self.state.totalPage > currentPageNo){
       ++currentPageNo;
       self.refs.uPageNo.value=currentPageNo;
      if(self.state.totalPage == currentPageNo){
        self.refs.next.className = "disabled";
      }
        self.refs.previous.className =""; 
        self.setState({isLoading:true})
        self.searchContactList(currentPageNo);
      }else{
        self.refs.uPageNo.value=currentPageNo;
        self.refs.next.className = "disabled";
       }
    },
 handleEnter:function(e){

  var self=this;
   var currentPageNo;
  if (e.charCode == 13 && e.target.value > 0) {
    currentPageNo=e.target.value
    if(self.state.totalPage >= currentPageNo){

      self.refs.next.className = "";

      if(self.state.totalPage == currentPageNo){
        self.refs.next.className = "disabled";
      }else if(self.state.totalPage == ONE || currentPageNo == ONE){
        self.refs.previous.className ="disabled";
      }
      if(currentPageNo > ONE){
        self.refs.previous.className ="";
      }
        self.setState({isLoading:true})
        self.searchContactList(currentPageNo);

    }
  }

 },
  handlePreviousPage:function(){
    var self=this;
    var currentPageNo=this.state.pageNo;
    if(currentPageNo > ONE){
       --currentPageNo;
       self.refs.uPageNo.value=currentPageNo;
         if(currentPageNo==ONE) 
         self.refs.previous.className = "disabled";
         else 
         self.refs.next.className = "";
       self.setState({isLoading:true})
       self.searchContactList(currentPageNo);
    }else{
       self.refs.uPageNo.value=currentPageNo;
       self.refs.previous.className = "disabled";
      }
    
   },
  renderRowResults:function(contactListData){
    var self=this;
    var contactList=this.bindDataFields(contactListData.result);
    var contactDataList=contactList.map(function(contactData){
           return(
             <tr key={contactData.contactId} className="clickable-row" onClick={this.handleViewContact.bind(this,contactData.contactId)}>
             <td>{contactData.contactId}</td>
            <td className="title-case"><span>{contactData.name}</span></td>
            <td>{contactData.email}</td>
            <td>{contactData.phone}</td>
            <td>{contactData.address}</td>       
          </tr>
          )
          }.bind(this));
          return contactDataList;
   },

  render:function(){
  
   var currentPageNo=this.state.pageNo;
    var contactList;
    var viewContact;
      if(this.state.isLoadingContact){
        viewContact=<Loader isLoading={this.state.isLoadingContact} />
      }else if(this.state.isViewContact){
        viewContact=<ContactView contactDetailData={commonUtils.createContactObj(this.state.contactDetailData)} handleCancelContactDetails={this.handleCancelContactDetails} isCancel={false} panelHead={this.state.panelHead}/>
      }
      if(this.state.isLoading){

        contactList=<tbody className=""><tr className="loader-tr"><td colSpan={4}><Loader isLoading={this.state.isLoading} /></td></tr></tbody>

      }else if(this.state.contactListData != null && this.state.contactListData.total > 0) {
      contactList=<tbody className="">
                     {!commonUtils.isNullOrEmpty(this.state.contactListData) ? this.renderRowResults(this.state.contactListData): ''}
                   </tbody>
      }
   
   return(

               <div className="merge-group">
                    <div className={"hide-"+this.state.isViewContact}>
                       <div className="header-label-size">Contact List</div>
                        <div className="merge-group-table-wrapper">
                            <table className="selectable-row table">
                              <thead className="table-header-color">
                                <tr>
                                  <th>ID</th>
                                  <th>Name</th>
                                  <th>Email</th>
                                  <th>Phone</th>
                                  <th>Address</th>
                                </tr>
                            </thead>
                                {contactList} 
                            </table> 
                            <div className="merge-group-pagination">
                                <ul className="pager">
                                 <li onClick={this.handlePreviousPage} id="previous" className="disabled" ref="previous"><a>Previous</a></li>
                                  <li>Page<input type="text" className="form-control-x" onKeyPress={this.handleEnter} defaultValue={currentPageNo} ref="uPageNo" id="uPageNo"/>
                                             of {this.state.totalPage}</li>
                                  <li onClick={this.handleNextPage} ref="next"><a>Next</a></li>
                                </ul>
                              <button type="button" className="btn create-contact-btn" ref="createBtn" onClick={this.handleCreateContact}>Create Contact</button>
                            </div>
                        </div>
                    </div>
                    {viewContact}
                </div>
      )
  }

});

module.exports = SearchContact;