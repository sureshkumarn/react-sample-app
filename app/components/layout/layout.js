var React = require('react');

var HeaderComponent=require('../header/header.js');
var FooterComponent = require('../footer/footer.js');
import navLinks from '../nav/nav'; 

var Layout  = React.createClass({
    getInitialState: function(){
        return null;
    },
    componentWillMount: function(){},
    componentDidMount: function(){},
    getPageName:function(){
        var pathName = this.props.location.pathname,
            className = 'site-pages';
        if(pathName.indexOf('login')!=-1){
            className += ' login';
        }
        return className;
    },
    render:function(){
        var className = this.getPageName();
        return(
            <div className={className}>
                <HeaderComponent navLinks={navLinks}/>
                <section className="main">{this.props.children}</section>
                <FooterComponent/>
            </div>
        );
    }
});

module.exports = Layout;