// Form field with all the necessary elements like validation, label
import React from 'react';

var Field = React.createClass({
    getInitialState: function(){
        return null;
    },
    onBlur:function(field,name){
        this.props.onBlur(field);
    },
    render: function(){
    	return(
    		<div className="form-group">
    			<label ref="label">{this.props.label}</label>
    			<input ref="field" className="form-control" type={this.props.type} onBlur={this.onBlur} defaultValue={this.props.initialValue}/>
    			<span ref="validationMsg" className={this.props.validationClass}>{this.props.validationMsg}</span>
    		</div>
    	); 
    }
});

module.exports = Field;