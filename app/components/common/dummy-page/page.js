// Form field with all the necessary elements like validation, label
import React from 'react';

var Page = React.createClass({
    getInitialState: function(){
        return null;
    },
    render: function(){
    	return(
    		<div><h1>Sample Page</h1></div>
    	); 
    }
});

module.exports = Page;