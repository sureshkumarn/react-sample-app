import React from 'react';
import Field from '../common/field/field';
import config from '../../config/config';
import utils from '../../utils/commonUtils';

var LoginPage = React.createClass({
    getInitialState: function(){
        var serverError = "";
        serverError = this.checkForInitialMessage();
        return {
            usernameValidationMsg:null,
            passwordValidationMsg:null,
            serverError:serverError
        };
    },
    componentWillMount: function(){},
    componentDidMount: function(){},
    loginMainPage:function(){},
    onBlur:function(field,name){
        var value = "", f;
        if(field=='username'){
            // check for username
            f = this.refs[field];
            value = f.refs['field'].value;
            if(value==""){
                this.setState({
                    usernameValidationMsg:config.messages.USERNAME_REQUIRED,
                    validationClass:"show danger"
                });
            } else if(value!=""){
                this.setState({
                    usernameValidationMsg:null,
                    validationClass:""
                });
            }
        } else if(field=='password'){
            // check for password
            // console.log(this.refs);
            f = this.refs[field];
            value = f.refs['field'].value;
            if(value==""){
                this.setState({
                    passwordValidationMsg:config.messages.PASSWORD_REQUIRED,
                    validationClass:"show danger"
                });
            } else if(value!=""){
                this.setState({
                    passwordValidationMsg:null
                });
            }
        }
    },
    getAccessToken:function(e){
        var username, password, hash;
        username = this.refs.username.refs['field'].value;
        password = this.refs.password.refs['field'].value;
        e.preventDefault();
        hash = md5(password);
        hash = btoa(username+":"+hash);        
        $.ajax({
            type:"POST",
            dataType:"json",
            url:config.apis.tokenApi.url,                
            beforeSend: function (request)
            {
                request.setRequestHeader("Authorization", "Basic "+hash);
                request.setRequestHeader("From","xxx@gmail.com");
            },             
            headers:{
                "Content-Type":"application/json",
                "X-Request-ID":"23455",
                "X-Application-ID":"2"
            },
            success:function(data){
                if(!utils.isNullOrEmpty(data.token)){
                    utils.setAccessKey(data.token,username);   
                    window.location = window.location.pathname;
                }
                //console.log(data);
            }.bind(this),
            error:function(err){                
                // show error
                var error = <span>Unable to login. Check username and password!</span>;
                this.setState({
                    serverError:error
                });
                //console.log('/#/merge-contact/s/'+data.id+'/contact/'+selectedContacts.toString());
                console.log(err);
            }.bind(this)
        });
    },
    checkForInitialMessage:function(){
        var params = this.props.params;
        if(!utils.isNullOrEmpty(params)){
            if(params["m"]=='lo'){
                return("You have been logged out successfully!");
            } else if(params['m']=='se') {
                return("Your session has expired. Please login again!");
            }
        }
        return "";
    },
    render:function(){
        var serverError = this.state.serverError;
        return(
            <div className="login-form">
                <form>
                    <span className="danger show">{serverError}</span>
                    <Field ref="username" validationMsg={this.state.usernameValidationMsg} onBlur={this.onBlur.bind(this,'username')} label="Username" type="text"/>
                    <Field ref="password" validationMsg={this.state.passwordValidationMsg} onBlur={this.onBlur.bind(this,'password')} label="Password" type="password"/>
                    <button onClick={this.getAccessToken} className="btn">Login</button>
                </form>
            </div>
        );
    }
});

module.exports = LoginPage;