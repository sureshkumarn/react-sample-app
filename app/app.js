// import library modules
import { Router, Route, IndexRoute, hashHistory } from 'react-router';
import utils from './utils/commonUtils.js';
import React from 'react';
import ReactDOM from 'react-dom';

// import app modules
import Layout from './components/layout/layout.js';
import HomePage from './components/home/home.js';
import NotFound from './components/not-found/not-found.js';
import LoginPage from './components/login/login.js';
import SearchContactComponent from './components/search-contact/search-contact.js';
import dummyPage from './components/common/dummy-page/page';

// App level configurations 
/*
$.ajaxSetup({        
   beforeSend: function (request)
            {
                request.setRequestHeader("Authorization","Bearer "+utils.getAccessKey(true)); 
                request.setRequestHeader("From",utils.getAccessUserId(true)); 
                              
            }  
});
*/

ReactDOM.render(
    <Router history={hashHistory}>
        <Route component={Layout}>
            <Route path="/" component={HomePage}></Route>
            <Route path="/page/:p1" component={dummyPage}></Route>
            <Route path="/login" component={LoginPage}></Route>
            <Route path="/search-contact" component={SearchContactComponent}></Route>
            <Route path="*" component={NotFound}></Route>
        </Route>
    </Router>,
    document.querySelector("#appCDI")
);

