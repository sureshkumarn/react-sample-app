import config from '../config/config';
import Cookies from 'cookies-js';

var CommonUtils = {
    consts : {
        ONE : 1,
        ZERO : 0,
        RECORDS_PER_PAGE:10
    },
    fields: {
        createdAt:"Created At",
        localModifiedBy: "Locally Modified By",
        localModifiedAt: "Locally Modified At",
        systemId: "System Name",
        apiName: "Api Name",
        version: "version",
        modifiedBy: "Modified By"
    },
    isNullOrEmpty: function(val) {
        return (val === undefined || val == null || val.length <= 0 || val === 0) ? true : false;
    },
    totalPages: function(totalRecords,nofpage) {
        var totalPage = Math.ceil((totalRecords / nofpage));
        return (totalPage);
    },
    getFormattedDate:function(str){
        if(!this.isNullOrEmpty(str)){
            var d = new Date(str);
            return d.getDate()+"-"+(d.getMonth()+1)+'-'+d.getFullYear();    
        } else {
            return "";
        }
    },
    getFormattedTime: function(str){
        if(!this.isNullOrEmpty(str)){
            var d = new Date(str);
            return d.getHours()+ ":" +d.getMinutes()+":"+d.getSeconds()+" "+d.getDate()+"-"+(d.getMonth()+1)+'-'+d.getFullYear();
        } else {
            return "";
        }
    },
    getCurrentDate:function(){
        var currentDate = new Date().toISOString();
        var dateTimeGMT=currentDate.split(".", 1);
       
        return String(dateTimeGMT+"Z"); 
    },
    validateEmail:function(email) {
        if(!this.isNullOrEmpty(email)) { 
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }else{
            return true;
        }
    },
    getSystemName: function(systemId,systemDetails){
        var i;
        for(i=0;i<systemDetails.length;i++){
            if(systemDetails[i].id == systemId){
                return systemDetails[i].value;
            }
        }
        return "";
        //console.log(systemDetails);
    },
    createEmptyContactObj: function() {
        return {
            id: 0,
            metadata: {
                localModifiedAt: null,
                localModifiedBy: null,
                localCreatedAt: null,
                version: 0
            },
            attributes: {
                name: {
                    title: null,
                    givenName: null,
                    familyName: null,
                    preferredName: null
                },
                gender: null,
                birthDate: null,
                age: {
                    capturedValue: null,
                    capturedAt: null
                },
                maritalStatus: null,
                nationality: null,
                nationality2: null,
                nri: 0,
                occupation: null,
                photoFilepath: null,
                prStatus: null,
                doNotDisturb: {
                    doNotCall: false,
                    doNotSms: false,
                    doNotEmail: false,
                    doNotPostmail: false
                },
                countryOfResidence: null,
                txColMap: {
                    occupation: null,
                    gender: null,
                    nri: null,
                    prStatus: null,
                    countryOfResidence: null,
                    birthDate: null,
                    nationality: null,
                    doNotDisturb: null,
                    nationality2: null,
                    name: null,
                    photoFilepath: null,
                    age: null
                },
            },
            phones: {
                personalMobile1: {
                    number: null,
                    verified: 0
                },
                personalMobile2: {
                    number: null,
                    verified: 0
                },
                personalMobile3: {
                    number: null,
                    verified: 0
                },
                workMobile1: {
                    number: null,
                    verified: 0
                },
                workMobile2: {
                    number: null,
                    verified: 0
                },
                workMobile3: {
                    number: null,
                    verified: 0
                },
                homePhone1: {
                    number: null,
                    verified: 0
                },
                homePhone2: {
                    number: null,
                    verified: 0
                },
                homePhone3: {
                    number: null,
                    verified: 0
                },
                workPhone1: {
                    number: null,
                    verified: 0
                },
                workPhone2: {
                    number: null,
                    verified: 0
                },
                workPhone3: {
                    number: null,
                    verified: 0
                },
                workFax1: {
                    number: null,
                    verified: 0
                },
                txColMap: {
                    workMobile1: 0,
                    workMobile2: 0,
                    workPhone2: 0,
                    personalMobile1: 0,
                    workPhone1: 0,
                    workFax1: 0,
                    personalMobile2: 0,
                    homePhone1: 0,
                    personalMobile3: 0,
                    homePhone2: 0,
                    workMobile3: 0
                }
            },
            emails: {
                personalEmail1: {
                    address: null,
                    verified: 0
                },
                personalEmail2: {
                    address: null,
                    verified: 0
                },
                workEmail1: {
                    address: null,
                    verified: 0
                },
                workEmail2: {
                    address: null,
                    verified: 0
                },
                txColMap: {
                    personalEmail1: 0,
                    personalEmail2: 0,
                    workEmail1: 0,
                    workEmail2: 0
                }
            },
            addresses: {
                homeAddress1: {
                    addressLine1: null,
                    addressLine2: null,
                    townCity:null,
                    stateProvinceRegion: null,
                    postalCode: null,
                    country: null
                },
                homeAddress2: {
                    addressLine1: null,
                    addressLine2: null,
                    townCity:null,
                    stateProvinceRegion: null,
                    postalCode: null,
                    country: null
                },
                workAddress1: {
                    addressLine1: null,
                    addressLine2: null,
                    townCity:null,
                    stateProvinceRegion: null,
                    postalCode: null,
                    country: null
                },
                workAddress2: {
                    addressLine1: null,
                    addressLine2: null,
                    townCity:null,
                    stateProvinceRegion: null,
                    postalCode: null,
                    country: null
                },
                txColMap: {
		            homeAddress2: 0,
		            homeAddress1: 0,
		            workAddress1: 0,
		            workAddress2: 0
		        }
            },
            localContactMap: [{
		        systemId: 0,
		        localContactId: null,
		        contactId: 0,
		        prime: 0
		    }],
		    txLog: [{
		        logId: 0,
		        createdAt: 0,
		        localModifiedBy: null,
		        localModifiedAt: 0,
		        systemId: 0,
		        apiName: null,
		        version: 0
		    }]
        };
    },
    getConstants: function(connstantList,successFn){
        var constants;
        $.ajax({
            type:config.apis.constantsApi.options.method,
            dataType:"json",
            url:config.apis.constantsApi.url,
            headers:config.apis.constantsApi.options.headers,
            data:JSON.stringify({"names":connstantList}),
            success:successFn,
            error:function(err){
                console.log(err);
            }
        });
        return constants;
    },
    createContactObj: function(obj) {
        //console.log('actual');
      // console.log('UTIL',obj);
    	var emptyObj = this.createEmptyContactObj();
    	var contactObj = $.extend(true,emptyObj,obj);
        //console.log('merged');
        //console.log(contactObj);
        return contactObj;
    },
    defaultCountry: 'in',
    getDefaultCountryObj: function(){
        var country, 
            countryList = CountryList.allCountries;
            
        for(country in countryList){
            if(countryList[country].iso2==this.defaultCountry)
                return countryList[country];
        }
    },
    validateField : function(value,valueType){
        var errMsg = "", flag = false, that = this;

        var validateName = function(name){
            var givenName = name.givenName || "",
                familyName = name.familyName || "",
                iChars = "!@#$%^&*()+=-[]\\\';,./{}|\":<>?";

            if(givenName!=""||familyName!=""){
                if(givenName.length+familyName.length>2){
                    if((givenName.indexOf(iChars)!=-1)&&(familyName.indexOf(iChars)!=-1)){
                        errMsg = "Invalid Name!";
                    }
                } else {
                        errMsg = "Invalid Name!";
                }
            } else {
                errMsg = "Invalid Name!";
            }
        }
        var validateAge = function(age){
            if((age!="")&&(isNaN(age)||age<0||age>150)){
                errMsg = "Age should be between 0 to 150!"
            }
        }
        switch(valueType){
            case 'name':
                validateName(value);
                break;
            case 'email':
                flag = this.validateEmail(value);
                if(!flag){
                    errMsg = "Invalid Email!"
                }
                break;
            case 'age':
                validateAge(value);
                break;
        }
        return errMsg;
    },
    validatePhoneNumber: function(number){
        var phoneNumber,
            indianNumber,
            usNumber, 
            cc, 
            countryCode,
            isValid,
            formattedNumber ="",
            countryObj = {
                name:"",
                iso2:"zz"
            };

        var getCountryDetails = function(countryCode){
            countryCode = countryCode.toLowerCase();
            var country, 
                countryList = CountryList.allCountries;
            
            for(country in countryList){
                if(countryList[country].iso2==countryCode){
                    return countryList[country];
                }
            }
            return {
                dialCode:"",
                iso2:"zz",
                name:"Unknown"
            };
        }

        var validateForUsNumber = function(number){
            var usNumber = '+1'+number,
                usNum,
                cc;
            usNum = phoneUtil.prase(usNumber);
            try {
                cc = phoneUtil.getRegionCodeForNumber(usNum);
                isValid = phoneUtil.isValidNumberForRegion(usNum,cc);
                return isValid;
            } catch(e){
                return false;
            }
        }

        var validateForInNumber = function(number){
            var indianNumber = '+91'+number,
                inNum,
                cc;
            inNum = phoneUtil.parse(indianNumber);
            try {
                cc = phoneUtil.getRegionCodeForNumber(inNum);
                isValid = phoneUtil.isValidNumberForRegion(inNum,cc);
                return isValid;
            } catch(e){
                return false;
            }
        }
        if(number!=='+91'&&!this.isNullOrEmpty(number)){
            if(number[0]!=='+'){
                if(validateForInNumber(number)){
                    number = '+91'+number;
                } else if(validateForUsNumber){
                    number = '+1'+number;
                }
            }
            try{
                phoneNumber = phoneUtil.parse(number,"");
                cc = phoneUtil.getRegionCodeForNumber(phoneNumber);
                if(cc){
                    countryCode = cc.toLowerCase();
                } else {
                    countryCode = 'zz';
                }
                countryObj = getCountryDetails(countryCode);
                isValid = phoneUtil.isValidNumberForRegion(phoneNumber,countryCode);
                formattedNumber =  phoneUtil.format(phoneNumber, PNF.E164);
            } catch(e){
                console.log(e);
                
                return {
                    isValid:false,
                    countryObj:countryObj,
                    number:formattedNumber
                }
            }
        } else {
            isValid = false;
            countryObj = {
                name:"",
                iso2:"zz"
            },
            formattedNumber = "";
        }
        return {
            isValid:isValid,
            countryObj:countryObj,
            number:formattedNumber
        }
    },
    setAccessKey:function(key,userId){
        var a;
        Cookies.set('k',key, {expires:3600});
        Cookies.set('a','u');
        Cookies.set('userId',userId,{expires:3600});
        //console.log(a);
        return true;
    },
    getAccessUserId:function(){
        var userId;
        userId = Cookies.get('userId');
        if(!this.isNullOrEmpty(userId)){
            return userId;
        } 
    },
    getAccessKey:function(redirect){
        var token, auth;
        token = Cookies.get('k');
        if(!this.isNullOrEmpty(token)){
            return token;
        } else if(redirect=='login'){
            auth = Cookies.get('a');
            if(!this.isNullOrEmpty(auth)&&auth=='u'){
                // token expired. redirect to login again with session expired message.
                window.location = window.location.pathname+'#/login/m/se';
            } else {
                // token expired. redirect to login again without session expired message.
                window.location = window.location.pathname+'#/login';
            }
        } else if(redirect){
            // token expired. redirect to login again with session expired message.
            window.location = window.location.pathname+'#/login/m/se';
        } else {
            return false;
        }
    },
    removeAccessKey:function(){
        Cookies.expire('k');
        Cookies.expire('a');
        Cookies.expire('userId');
        window.location = window.location.pathname+'#/login/m/lo';
    }
}

module.exports = CommonUtils;
