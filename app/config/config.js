// App Level Config
import config from '../../config/config'; 
// statics
var loadingURL = window.location.hostname;
var Authtoken;

// Path Configs

var wwwroot = config.wwwroot,	
	appRoot = config.appRoot,
	domains_url = {
		"production":"https://api.isha.in",
		"stage":"https://qa-api.isha.in",
		"qa":"https://qa-api.isha.in",
		"qa-ib":"https://qa-ib.isha.in",
		"local":"http://localhost:1337/app/dummy-data"
	},
	headers = {
		"Content-Type":"application/json",			
		"X-Request-ID":"23455",
		"X-Application-ID":"2"
	},
	widget_headers = {
		"Content-Type":"application/json",
		"Authorization":Authtoken,
		"From":"user001",
		"X-Request-ID":"23455",
		"X-Application-ID":"2"
	};

var generateApis = function(host,api_path){
	var midPath,ibmidpath,tokenApi;
	if(host==domains_url['production']){
		 midPath = api_path||"/cdi/v1/";
		 ibmidpath="/cdi/v1/sync/syncprocess";
		 tokenApi="/auth/v1/token/issue";
		}
	else{	
	    midPath = api_path||"/v1/in/contact/api/";
	    ibmidpath="/v1/in/independent/api/syncprocess";
	    tokenApi="/v1/auth/api/token/issue";
	   }  
	return {
		"tokenApi":{
			"url":host+tokenApi,
			"options":{
				headers:headers
			}
		},
		"suggestMergeApi":{
			"url":host+midPath+"contact/merge/suggest",
			"options":{
				"headers":headers
			}
		},
		"antiMergeApi":{
			url:host+midPath+"contact/merge/antimatch",
			options:{
				method:'POST',
				headers:headers
			}
		},
		"mergeGroupApi": {
			"url":host+midPath+"contact/match-group/fetch",
			"options":{
				"headers":headers,
					method:'POST'
			}
		},
		"mergeContactApi": {
			url:host+midPath+"contact/merge",
			"options":{
				"headers":headers,
					method:'POST'
			}	
		},
		"constantsApi":{
			url:host+midPath+"constant/fetch",
			options:{
				method:"POST",
				headers:headers
			}
		},
		"contactsApi":{
			url:host+midPath+"contact/fetch",
			options:{
				method:'POST',
				headers:headers
			}
		},
		"systemApi": {
			"url":host+midPath+"system",
			"options":{
				"headers":headers
			}
		},		
		"integrationBusApi": {
			"url":host+ibmidpath,
			"options":{
				method: "GET",
				"headers":headers
			}
		},
		"ibRetryApi":{
			"url":host+ibmidpath,
			"options":{
				method:"POST",
				"headers":headers
			}	
		},
		"dataFixApi": {
			"url":host+midPath+"contact/error/fetch",
			"options":{
				 method:"POST",
				"headers":headers
			}

		},
		"searchWidgetContactApi": {
			"url":host+midPath+"contact/search",
			"options":{
				method:'POST',
				"headers":widget_headers
			}

		},
		"searchContactApi": {
			"url":host+midPath+"contact/search",
			"options":{
				method:'POST',
				"headers":headers
			}

		},
		"searchContactByGuidApi": {
			"url":host+midPath+"contact/",
			"options":{
				method:'GET',
				"headers":headers
			}

		},
		"updateContactApi": {
			"url":host+midPath+"contact/",
			"options":{
				method:"PUT",
				"headers":headers
			}
		},
		"createContactApi": {
			"url":host+midPath+"contact",
			"options":{
				method:"POST",
				"headers":headers
			}
		},
		"dataFixRemoveApi": {
			"url":host+midPath+"contact/data-fix/remove",
			"options":{
				 method:"POST",
				"headers":headers
			}

		},
		"updateDatafixContactApi": {
			"url":host+midPath+"contact/data-fix/update/",
			"options":{
				method:"PUT",
				"headers":headers	
			}
		},
	};
}

var api = { 
	"local": {
		"suggestMergeApi":{
			"url":domains_url.local+"/suggest-merge.json",
			"options":{}
		},
		"mergeGroupApi":{
			"url":domains_url.local+"/listAllMatchGroups.json",
			"options":{
				method:'GET'
			}
		},
		mergeContactApi: {
			url:domains_url.stage+"/contact/merge",
			"options":{
				"headers":headers,
				method:'POST'
			}	
		},
		"constantsApi":{
			url:domains_url.local+"/constants.js",
			options:{
				method:"GET",
				headers:{}
			}
		},
		"contactsApi":{
			"url":domains_url.local+"/multiple-contact-id-ib.json",
			"options":{
				method:'GET'
			}
		},
		"saveMergedContactApi":{
			url:domains_url.local+"/contact",
			options:{
				method:'GET'
			}
		},
		"systemApi": {
			"url":domains_url.local+"/system-meta.json",
			"options":{
				"headers":{}
			}
		},
		"integrationBusApi": {
			"url":domains_url.local+"/integration-bus.json",
			"options":{
				"headers":{}
			}
		},
		"ibRetryApi":{
			"url":domains_url.local+"/ibretry.json",
			"options":{
				method:"GET",
				"headers":{}
			}	
		},
		"dataFixApi": {
			"url":domains_url.local+"/data-fix.json",
			"options":{
				method:'GET',
				"headers":{}
			}
		},
		"createContactApi": {
			"url":domains_url.local+"/contact",
			"options":{
				method:'GET',
				"headers":{}
			}
		},
		"relationShipApi": {
			"url":domains_url.local+"/relation-ship.json",
			"options":{
				method:'GET',
				"headers":{}
			}
		},
		"antiMergeApi":{
			url:domains_url.local+"/contact/merge/antimatch",
			options:{
				method:'POST',
				headers:{}
			}
		},
		"searchContactApi": {
			"url":domains_url.local+"/searchresult.json",
			"options":{
				method:'GET',
				"headers":{}
			}
		},
		"updateContactApi": {
			"url":domains_url.local+"/singlecontact.json",
			"options":{
				method:"GET",
				"headers":{}	
			}
		},
			"updateDatafixContactApi": {
			"url":domains_url.local+"/singlecontact.json",
			"options":{
				method:"PUT",
				"headers":{}	
			}
		},
		
	}
}

if( loadingURL == ''){
	var apiRoot = generateApis(domains_url["production"]);
}else{
	var apiRoot = generateApis(domains_url["qa-ib"]);
}

var appConfig = {
	"errors":{
		"NO_RECORDS_FOUND_MSG": "No Records Found",
		"SYSTEM_DETAILS_NOT_FOUND": "Unable to fetch system details!",
		"SERVER_ERROR": "Server is down. Please try again later!"
	},
	messages:{
		"NO_RECORDS_FOUND": "No Records Found",
		"NAME_SEARCH_WARNING":"The search name should not be less than 3 characters",
		"SEARCH_WARNING": "Please Enter Name/Email/Phone Values to Search Contact",
		"NO_MSG":"",
		"FIRST_NAME_REQUIRED":"Please Enter First Name",
		"NAME_VALID_MSG":"First Name and Last Name should not be less than 3 characters",
		"EMAIL_VALIDATION":"Please enter valid email address",
		'AGE_VALIDATION_MSG':"Age should be between 1 to 150",
		"USERNAME_REQUIRED" : "Please enter username",
		"PASSWORD_REQUIRED" : "Please enter password"
	},
	"constants":{
		"WARNING":"warning",
		"INFO":"info",
		"DANGER":"danger",
		"SUCCESS":"success",
		"VIEW_CONTACT":"View Contact",
		"CREATE_CONTACT":"Create Contact",
		"CREATED":"Created",
		"UPDATED":"Updated",
		"INDIA":"in",
	},
	"contact":{
        name:"Name",
        title:"Title",
        givenName: "First Name",
        familyName: "Last Name",
        preferredName:"Preferred Name",
        gender: "Gender",
        birthDate: "Birth Date",
        age:"Age",
        maritalStatus: "Marital Status",
        nationality: "Nationality",
        nationality2: "Nationality1",
        nri: "NRI",
        occupation: "Occupation",
        photoFilepath: "Phot File Path ",
        prStatus: "PR Status",
        doNotDisturb:"Do Not Disturb",
        countryOfResidence: "Country Of Residence",
   
        personalMobile1:"Personal Mobile1",
        personalMobile2:"Personal Mobile2",
        personalMobile3:"Personal Mobile3",
        workMobile1:"Work Mobile1" ,
        workMobile2:"Work Mobile2" ,
        workMobile3:"Work Mobile3" ,
        homePhone1:"Home Phone1",
        homePhone2: "Home Phone2",
        homePhone3: "Home Phone3",
        workPhone1:"Work Phone1",
        workPhone2: "Work Phone2",
        workPhone3:"Work Phone3",
        workFax1:"Work Fax",
        
        personalEmail1:"Personal Email1",
        personalEmail2:"Personal Email2",
        workEmail1:"Work Email1",
        workEmail2:"Work Email2",
        homeAddress1:"Home1",
        homeAddress2:"Home2",
        workAddress1:"Work1",
        workAddress2:"Work2"
	},
  	"salesforce":{
  	   "viewContactURL":"*",
	   "createContactURL":"*"
    },
	"wwwroot":wwwroot,
	"appRoot":appRoot,
	"apis": apiRoot
}

module.exports = appConfig;