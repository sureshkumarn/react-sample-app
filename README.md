# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Sample React App
* V-0.0.0

### How do I get set up? ###

* Install sass globally
npm install -g sass

* checkout the repository and run the following command inside the cdi-ui folder
npm install 

* run the following command in the terminal 
gulp sass
gulp serve-and-watch

* page will be served in http://localhost:1337

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact