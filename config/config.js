// App level configurations

var wwwroot = process.cwd()+'wwwroot/';

// Path Configs
var config = {
	wwwroot : wwwroot,
	appRoot : wwwroot+'app/'
}

module.exports = config;
