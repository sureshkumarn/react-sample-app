/*
 * Task Automation to make my life easier.
 * Author: Jean-Pierre Sierens
 * ===========================================================================
 */

// declarations, dependencies
// ----------------------------------------------------------------------------
var $ = require('gulp-load-plugins')({ lazy: true });
var babelify = require('babelify');
var browserify = require('browserify');
var colors = $.util.colors;
var connect = require('gulp-connect');
var source = require('vinyl-source-stream');
var gulp = require('gulp');
var gulpClean = require('gulp-clean');
var gutil = require('gulp-util');
var open = require('gulp-open');
var minifyCss = require('gulp-minify-css');
var runSequence = require('run-sequence');
var sass = require('gulp-sass');

var config = require('./gulp.config')();

// External dependencies you do not want to rebundle while developing,
// but include in your application deployment
var dependencies = [
    'react',
    'react-dom',
    'react-router',
    'underscore',
    'whatwg-fetch',
    'deep-equal'
];
// keep a count of the times a task refires
var scriptsCount = 0;

// Gulp tasks
// ----------------------------------------------------------------------------

/* ---- START DIST TASK ----- */
gulp.task('dist', function() {
    runSequence('clean', 'build', 'del');
});

//delete temp folder
gulp.task('clean', function() {
    return gulp.src('dist', { read: false }).pipe(gulpClean());
})

gulp.task('build', function() {
    runSequence('scripts', 'copy-js', 'copy-vendor-js', 'copy-css', 'copy-assets', 'copy-html');
    console.log('dist build');
});

gulp.task('copy-js', function() {
    return gulp
        .src('wwwroot/js/*.js')
        .pipe(gulp.dest('dist/js'))
});

gulp.task('copy-vendor-js', function() {
    return gulp
        .src('wwwroot/vendor/*.js')
        .pipe(gulp.dest('dist/vendor'))
});

gulp.task('copy-css', function() {
    return gulp
        .src('wwwroot/css/*.css')
        .pipe(gulp.dest('temp/css'))
        .pipe(minifyCss({ compatibility: 'ie9' }))
        .pipe(gulp.dest('dist/css'));
});

gulp.task('copy-assets', function() {
    return gulp
        .src('wwwroot/assets/**/*')
        .pipe(gulp.dest('dist/assets'));
});

gulp.task('copy-html', function() {
    return gulp
        .src('wwwroot/*.html')
        .pipe(gulp.dest('dist/'));
});

//delete temp folder
gulp.task('del', function() {
    return gulp.src('temp', { read: false }).pipe(gulpClean());
})

/* ---- END DIST TASK ----- */

gulp.task('scripts', function() {
    bundleApp(false);
});

gulp.task('deploy', function() {
    bundleApp(true);
});

gulp.task('html', function() {
    gulp.src('./wwwroot/*.html')
        .pipe(connect.reload());

});

gulp.task('open', function() {
    gulp.src(__filename)
        .pipe(open({ uri: "http://localhost:" + config.port + "/" }));
});

gulp.task('watch', function() {

    var watcher = gulp.watch(['./app/**/*.js'], { debounceDelay: 300 }, function() {
        runSequence('scripts');
    });

    watcher.on('change', function(event) {
        log('File ' + colors.red(event.path) + ' was ' + event.type + ', running tasks...');
    });
    //gulp.watch(['./wwwroot/app/**/*.js'], ['scripts']);
});


gulp.task('serve', function() {
    connect.server({
        root: 'wwwroot',
        port: 1337,
        livereload: true
    });

});


// When running 'gulp' on the terminal this task will fire.
// It will start watching for changes in every .js file.
// If there's a change, the task 'scripts' defined above will fire.
//gulp.task('default', ['scripts','watch','serve']);

gulp.task('serve-and-watch', function() {
    runSequence('scripts', 'watch', 'serve', 'open');
});

/*************** SASS TASKS *******************/

gulp.task('sass', function() {
    gulp.src(config.sass.in)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(config.sass.out));
});

gulp.task('sass:watch', function() {
    gulp.watch(config.sass.watch, ['sass']);
});

/*************** SASS TASKS END *******************/

// Private Functions
// ----------------------------------------------------------------------------
function bundleApp(isProduction) {
    scriptsCount++;
    // Browserify will bundle all our js files together in to one and will let
    // us use modules in the front end.
    var appBundler = browserify({
        entries: ['app/app.js'],
        debug: true
    })

    // If it's not for production, a separate vendors.js file will be created
    // the first time gulp is run so that we don't have to rebundle things like
    // react everytime there's a change in the js file
    if (!isProduction && scriptsCount === 1) {
        // create vendors.js for dev environment.
        browserify({
                require: dependencies,
                debug: true
            })
            .bundle()
            .on('error', gutil.log)
            .pipe(source('vendors.js'))
            .pipe(gulp.dest('./wwwroot/js/'));
    }
    if (!isProduction) {
        // make the dependencies external so they dont get bundled by the 
        // app bundler. Dependencies are already bundled in vendor.js for
        // development environments.
        dependencies.forEach(function(dep) {
            appBundler.external(dep);
        })
    }

    appBundler
    // transform ES6 and JSX to ES5 with babelify
        .transform("babelify")
        .bundle()
        .on('error', gutil.log)
        .pipe(source('bundle.js'))
        .pipe(gulp.dest('./wwwroot/js/')).
    on('end', function() {
        runSequence('html');
    });
}

/**
 * Log a message
 * @param   {Object} msg    Message to log.
 */
function log(msg) {
    if (typeof(msg) === 'object') {
        for (var item in msg) {
            if (msg.hasOwnProperty(item)) {
                $.util.log(colors.blue(msg[item]));
            }
        }
    } else {
        $.util.log(colors.blue(msg));
    }
}
